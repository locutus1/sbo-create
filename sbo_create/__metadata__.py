#!/usr/bin/python3
# -*- coding: utf-8 -*-

__prog__: str = 'sbo-create'
__author__: str = 'dslackw'
__copyright__: str = '2015-2024'
__version_info__: tuple = (2, 0, 8)
__version__: str = '{0}.{1}.{2}'.format(*__version_info__)
__license__: str = 'GNU General Public License v3 (GPLv3)'
__email__: str = 'dslackw@gmail.com'
__website__: str = 'https://gitlab.com/dslackw/sbo-create'
